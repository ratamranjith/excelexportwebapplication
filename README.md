# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This Application helps to update the data in the database by Manually Importing the excel file/Updating in the WebPage.
* [Learn ExcelExport Using Flask](https://bitbucket.org/ratamranjith/excelexportwebapplication)

### How do I get set up? ###
* Pre-Requirements:
1. MySql Installer
2. Python 2.7
   i.  Flask   - WebApplication
   ii. MySQLdb - DataBase
   iii. Xlrd   - ExcelSheet Read/Write
3. Excel Application - For Viewing the data